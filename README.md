# fyp-21-s2-12p-submission

This repository contains submission for FYP project fyp-21-s2-12p - Social Media Rumour Detection.

***


## News Skeptic
Our deployed website could be found [here.](http://35.240.239.227:8000/)

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Installation
Installation guide is available for each purpose.
    - Model building : will be located in the code base for models folder
    - Website deployment and CI/CD : will be located in the code base for website folder

## Support
Contact contactnewsskeptic@gmail.com for Support or business.

## Authors and acknowledgment
Contributors : 
| Name | Role |
| ------ | ------ |
| Hazwan| Data collection and model building |
| Wah | Website |
| Matt | Model building |
| Ya Min | Model building |

